# FamilyTreeMaker Web App

# Development

## Install dependencies

```
npm install
```

## Run local server

```
npm start
```

## Make a production build

```
npm run build
```

### Test production build locally

```
serve -s build
```

### Use the CDK to push the changes to Amazon S3 + Cloudfront

See the Readme for the CDK package

### How does it look like?

![](docs/img/example.png)