import * as React from 'react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { styled } from '@mui/material/styles';

import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Unstable_Grid2';
import { Typography } from '@mui/material';

import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import BasicTable from './BasicTable';

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'firstName', headerName: 'First name', width: 130 },
  { field: 'lastName', headerName: 'Last name', width: 130 },
];

const rows = [
  { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
  { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
  { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
  { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
  { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
  { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
  { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
  { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
  { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  { id: 10, lastName: 'Melisandre', firstName: null, age: 150 },
  { id: 11, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
  { id: 12, lastName: 'Frances', firstName: 'Rossini', age: 36 },
  { id: 13, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  { id: 14, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  { id: 15, lastName: 'Melisandre', firstName: null, age: 150 },
  { id: 16, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
  { id: 17, lastName: 'Frances', firstName: 'Rossini', age: 36 },
  { id: 18, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
];

function DataTable() {
  return (
    <div style={{ height: 768 }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={12}
        rowsPerPageOptions={[12]}
      />
    </div>
  );
}



interface RenderTree {
  id: string;
  name: string;
  children?: readonly RenderTree[];
}

const data: RenderTree = {
  id: 'root',
  name: 'Parent',
  children: [
    {
      id: '1',
      name: 'Child - 1',
    },
    {
      id: '3',
      name: 'Child - 3',
      children: [
        {
          id: '4',
          name: 'Child - 4',
          children: [
            {
              id: '5',
              name: 'Child - 5',
            }
          ]
        },
      ],
    },
    {
      id: '6',
      name: 'Child - 6',
      children: [
        {
          id: '7',
          name: 'Child - 7',
          children: [
            {
              id: '8',
              name: 'Child - 8',
            }
          ]
        },
      ],
    },
  ],
};

function ImgMediaCard() {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component="img"
        alt="green iguana"
        // height="140"
        image="https://awoiaf.westeros.org/images/a/a0/Cristi_Balanescu_Jon_SnowGhost.png"
      />
      {/* <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Jon Snow
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Jon Snow is a fictional character in the A Song of Ice and Fire series of fantasy novels by American author George R. R. Martin, and its television adaptation Game of Thrones, in which he is portrayed by English actor Kit Harington. In the novels, he is a prominent point of view character
        </Typography>
      </CardContent> */}
      {/* <CardActions>
        <Button size="small">View</Button>
        <Button size="small">Edit</Button>
        <Button size="small">Share</Button>
      </CardActions> */}
    </Card>
  );
}


const Item = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(1),
  marginTop: theme.spacing(2)
}));


export default function Example() {
  const renderTree = (nodes: RenderTree) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => renderTree(node))
        : null}
    </TreeItem>
  );

  return (
    <Container maxWidth="lg">
      <Grid container spacing={2}>
        <Grid lg={4} md={4} xs={12}>
          <Item>
            <Typography gutterBottom variant="h5" component="div">
              Jon Snow
            </Typography>
            <ImgMediaCard />
          </Item>
        </Grid>
        <Grid lg={4} md={4} xs={12}>
          <Item>
            <BasicTable />
          </Item>
        </Grid>
        <Grid lg={4} md={4} xs={12}>
          <Item>
            <Typography variant="h6" gutterBottom>
              Family Tree
            </Typography>
            <TreeView
              aria-label="rich object"
              defaultCollapseIcon={<ExpandMoreIcon />}
              defaultExpanded={['root']}
              defaultExpandIcon={<ChevronRightIcon />}
              sx={{ flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
            >
              {renderTree(data)}
            </TreeView>
          </Item>
        </Grid>
        
        <Grid lg={12} md={12} xs={12}>
          <Item>
            <Typography variant="h6" gutterBottom>
              All Members
            </Typography>
            <DataTable />
          </Item>
        </Grid>
      </Grid>
    </Container>

  );
}
