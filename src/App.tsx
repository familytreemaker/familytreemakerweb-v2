import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import * as React from 'react';
import { Route, Routes } from "react-router-dom";
import Example from './example/Example';
import TopNavigation from './navigation/TopNavigation';
import SignIn from './sign-in/SignIn';
import SignUp from './sign-in/SignUp';
import Splash from './splash/Splash';
import About from './utility/About';
import Blog from './utility/Blog';

function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? 'dark' : 'light',
        },
      }),
    [prefersDarkMode],
  );

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />

      <TopNavigation />
      <Routes>
        <Route path="/" element={<Splash />} />
        <Route path="signup" element={<SignUp />} />
        <Route path="signin" element={<SignIn />} />
        <Route path="about" element={<About />} />
        <Route path="blog" element={<Blog />} />
        <Route path="example" element={<Example />} />
      </Routes>
    </ThemeProvider>
  );
}

export default App;
