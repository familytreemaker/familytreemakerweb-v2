import ParkIcon from '@mui/icons-material/Park';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import * as React from 'react';


import MenuIcon from '@mui/icons-material/Menu';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

export default function TopNavigation() {
  const pages = ['About', 'Blog', 'Example'];
  const buttons = ['Login', 'Sign Up'];

  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>

      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            href="/"
          >
            <ParkIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            FamilyTreeMaker
          </Typography>

          <Box sx={{ flexGrow: 0, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          <Box sx={{ flexGrow: 0, display: { xs: 'none', s: 'flex', md: 'flex', lg: 'flex', xl: 'flex' } }}>
            <Link href="/about" color="inherit" underline="hover" sx={{ mr: 2, mt: 1 }}>{pages[0]}</Link>
            <Link href="/blog" color="inherit" underline="hover" sx={{ mr: 2, mt: 1 }}>{pages[1]}</Link>
            <Link href="/example" color="inherit" underline="hover" sx={{ mr: 2, mt: 1 }}>{pages[2]}</Link>
            <Button href="/signin" color="inherit" variant="outlined" sx={{ mr: 2 }}>{buttons[0]}</Button>
            <Button href="/signup" color="inherit" variant="outlined" sx={{ mr: 2 }}>{buttons[1]}</Button>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
